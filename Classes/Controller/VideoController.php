<?php
namespace HIVE\HiveCptCntVideo\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_video" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * VideoController
 */
class VideoController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * videoRepository
     *
     * @var \HIVE\HiveCptCntVideo\Domain\Repository\VideoRepository
     * @inject
     */
    private $videoRepository = null;

    /**
     * action showVideo
     *
     * @return void
     */
    public function showVideoAction()
    {
        //get settings
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }
        //get plugin uid
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        //get videos
        $oVideos = $this->videoRepository->findByUidListOrderByListIfNotEmpty($aSettings['oHiveCptCntVideo']['main']['mn']);

        $this->view->assign('oVideos', $oVideos);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPluginUid', $iPluginUid);
    }
}
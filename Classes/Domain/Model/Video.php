<?php
namespace HIVE\HiveCptCntVideo\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_video" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Video
 */
class Video extends \HIVE\HiveExtArticle\Domain\Model\Article
{
    /**
     * format
     *
     * @var int
     */
    protected $format = 0;

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * largemp4
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $largemp4 = null;

    /**
     * largewebm
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $largewebm = null;

    /**
     * largeogv
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $largeogv = null;

    /**
     * smallmp4
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $smallmp4 = null;

    /**
     * smallwebm
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $smallwebm = null;

    /**
     * smallogv
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $smallogv = null;

    /**
     * videoid
     *
     * @var string
     */
    protected $videoid = '';

    /**
     * preload
     *
     * @var int
     */
    protected $preload = 0;

    /**
     * autoplay
     *
     * @var bool
     */
    protected $autoplay = false;

    /**
     * controls
     *
     * @var bool
     */
    protected $controls = false;

    /**
     * muted
     *
     * @var bool
     */
    protected $muted = false;

    /**
     * ratio
     *
     * @var int
     */
    protected $ratio = 0;

    /**
     * playbyclick
     *
     * @var bool
     */
    protected $playbyclick = false;

    /**
     * autopausebyothervideo
     *
     * @var bool
     */
    protected $autopausebyothervideo = false;

    /**
     * autopausebyvisibility
     *
     * @var bool
     */
    protected $autopausebyvisibility = false;

    /**
     * loopvideo
     *
     * @var bool
     */
    protected $loopvideo = false;

    /**
     * width
     *
     * @var int
     */
    protected $width = 0;

    /**
     * Returns the videoid
     *
     * @return string $videoid
     */
    public function getVideoid()
    {
        return $this->videoid;
    }

    /**
     * Sets the videoid
     *
     * @param string $videoid
     * @return void
     */
    public function setVideoid($videoid)
    {
        $this->videoid = $videoid;
    }

    /**
     * Returns the format
     *
     * @return int format
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Sets the format
     *
     * @param int $format
     * @return void
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the preload
     *
     * @return int preload
     */
    public function getPreload()
    {
        return $this->preload;
    }

    /**
     * Sets the preload
     *
     * @param int $preload
     * @return void
     */
    public function setPreload($preload)
    {
        $this->preload = $preload;
    }

    /**
     * Returns the autoplay
     *
     * @return bool $autoplay
     */
    public function getAutoplay()
    {
        return $this->autoplay;
    }

    /**
     * Sets the autoplay
     *
     * @param bool $autoplay
     * @return void
     */
    public function setAutoplay($autoplay)
    {
        $this->autoplay = $autoplay;
    }

    /**
     * Returns the boolean state of autoplay
     *
     * @return bool
     */
    public function isAutoplay()
    {
        return $this->autoplay;
    }

    /**
     * Returns the controls
     *
     * @return bool $controls
     */
    public function getControls()
    {
        return $this->controls;
    }

    /**
     * Sets the controls
     *
     * @param bool $controls
     * @return void
     */
    public function setControls($controls)
    {
        $this->controls = $controls;
    }

    /**
     * Returns the boolean state of controls
     *
     * @return bool
     */
    public function isControls()
    {
        return $this->controls;
    }

    /**
     * Returns the muted
     *
     * @return bool $muted
     */
    public function getMuted()
    {
        return $this->muted;
    }

    /**
     * Sets the muted
     *
     * @param bool $muted
     * @return void
     */
    public function setMuted($muted)
    {
        $this->muted = $muted;
    }

    /**
     * Returns the boolean state of muted
     *
     * @return bool
     */
    public function isMuted()
    {
        return $this->muted;
    }

    /**
     * Returns the ratio
     *
     * @return int $ratio
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Sets the ratio
     *
     * @param int $ratio
     * @return void
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
    }

    /**
     * Returns the largemp4
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference largemp4
     */
    public function getLargemp4()
    {
        return $this->largemp4;
    }

    /**
     * Sets the largemp4
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $largemp4
     * @return void
     */
    public function setLargemp4(\TYPO3\CMS\Extbase\Domain\Model\FileReference $largemp4)
    {
        $this->largemp4 = $largemp4;
    }

    /**
     * Returns the largewebm
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference largewebm
     */
    public function getLargewebm()
    {
        return $this->largewebm;
    }

    /**
     * Sets the largewebm
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $largewebm
     * @return void
     */
    public function setLargewebm(\TYPO3\CMS\Extbase\Domain\Model\FileReference $largewebm)
    {
        $this->largewebm = $largewebm;
    }

    /**
     * Returns the largeogv
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference largeogv
     */
    public function getLargeogv()
    {
        return $this->largeogv;
    }

    /**
     * Sets the largeogv
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $largeogv
     * @return void
     */
    public function setLargeogv(\TYPO3\CMS\Extbase\Domain\Model\FileReference $largeogv)
    {
        $this->largeogv = $largeogv;
    }

    /**
     * Returns the smallmp4
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $smallmp4
     */
    public function getSmallmp4()
    {
        return $this->smallmp4;
    }

    /**
     * Sets the smallmp4
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $smallmp4
     * @return void
     */
    public function setSmallmp4(\TYPO3\CMS\Extbase\Domain\Model\FileReference $smallmp4)
    {
        $this->smallmp4 = $smallmp4;
    }

    /**
     * Returns the smallwebm
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $smallwebm
     */
    public function getSmallwebm()
    {
        return $this->smallwebm;
    }

    /**
     * Sets the smallwebm
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $smallwebm
     * @return void
     */
    public function setSmallwebm(\TYPO3\CMS\Extbase\Domain\Model\FileReference $smallwebm)
    {
        $this->smallwebm = $smallwebm;
    }

    /**
     * Returns the smallogv
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $smallogv
     */
    public function getSmallogv()
    {
        return $this->smallogv;
    }

    /**
     * Sets the smallogv
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $smallogv
     * @return void
     */
    public function setSmallogv(\TYPO3\CMS\Extbase\Domain\Model\FileReference $smallogv)
    {
        $this->smallogv = $smallogv;
    }

    /**
     * Returns the autopausebyothervideo
     *
     * @return bool $autopausebyothervideo
     */
    public function getAutopausebyothervideo()
    {
        return $this->autopausebyothervideo;
    }

    /**
     * Sets the autopausebyothervideo
     *
     * @param bool $autopausebyothervideo
     * @return void
     */
    public function setAutopausebyothervideo($autopausebyothervideo)
    {
        $this->autopausebyothervideo = $autopausebyothervideo;
    }

    /**
     * Returns the boolean state of autopausebyothervideo
     *
     * @return bool
     */
    public function isAutopausebyothervideo()
    {
        return $this->autopausebyothervideo;
    }

    /**
     * Returns the autopausebyvisibility
     *
     * @return bool $autopausebyvisibility
     */
    public function getAutopausebyvisibility()
    {
        return $this->autopausebyvisibility;
    }

    /**
     * Sets the autopausebyvisibility
     *
     * @param bool $autopausebyvisibility
     * @return void
     */
    public function setAutopausebyvisibility($autopausebyvisibility)
    {
        $this->autopausebyvisibility = $autopausebyvisibility;
    }

    /**
     * Returns the boolean state of autopausebyvisibility
     *
     * @return bool
     */
    public function isAutopausebyvisibility()
    {
        return $this->autopausebyvisibility;
    }

    /**
     * Returns the playbyclick
     *
     * @return bool $playbyclick
     */
    public function getPlaybyclick()
    {
        return $this->playbyclick;
    }

    /**
     * Sets the playbyclick
     *
     * @param bool $playbyclick
     * @return void
     */
    public function setPlaybyclick($playbyclick)
    {
        $this->playbyclick = $playbyclick;
    }

    /**
     * Returns the boolean state of playbyclick
     *
     * @return bool
     */
    public function isPlaybyclick()
    {
        return $this->playbyclick;
    }

    /**
     * Returns the loopvideo
     *
     * @return bool $loopvideo
     */
    public function getLoopvideo()
    {
        return $this->loopvideo;
    }

    /**
     * Sets the loopvideo
     *
     * @param bool $loopvideo
     * @return void
     */
    public function setLoopvideo($loopvideo)
    {
        $this->loopvideo = $loopvideo;
    }

    /**
     * Returns the boolean state of loopvideo
     *
     * @return bool
     */
    public function isLoopvideo()
    {
        return $this->loopvideo;
    }

    /**
     * Returns the width
     *
     * @return int $width
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Sets the width
     *
     * @param int $width
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }
}

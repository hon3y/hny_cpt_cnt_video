<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$sModel = 'tx_hivecptcntvideo_domain_model_video';

$GLOBALS['TCA'][$sModel]['columns']['title'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.title',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];


$GLOBALS['TCA'][$sModel]['columns']['text'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.text',
    'config' => [
        'type' => 'text',
        'cols' => 40,
        'rows' => 15,
        'eval' => 'trim',
    ],
    'defaultExtras' => 'richtext:rte_transform'
];

$GLOBALS['TCA'][$sModel]['columns']['format']['config']['items'] = array(
    array('---', 0),
    array('file', 1),
    array('youtube', 2),
    array('vimeo', 3)
);

$GLOBALS['TCA'][$sModel]['columns']['preload']['config']['items'] = array(
    array('auto', 0),
    array('disable', 1),
    array('metadata', 2)
);

$GLOBALS['TCA'][$sModel]['columns']['ratio']['config']['items'] = array(
    array('TV (16:9)', 0),
    array('TV Classic (4:3)', 1),
    array('Square (1:1)', 2),
    array('Widecreen Cinema (1.85:1)', 3),
    array('Anamorphic (2.39:1)', 4)
);

$GLOBALS['TCA'][$sModel]['columns']['width']['config']['items'] = array(
    array('100%', 0),
    array('50%', 1),
    array('33%', 2),
    array('25%', 3)
);


$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = ' --div--;General,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,
                                        ,--div--;Title,title,
                                        ,--div--;Video,format,
                                        ,--palette--;Large Video Files;pallete_file_large,
                                        ,--palette--;Small Video Files;pallete_file_small,
                                        videoid,
                                        ,--palette--;Settings;pallete_settings
                                        ,--palette--;my_palette_label;pallete_name_two
                                        ,--div--;Image,image,
                                        ';

//TODO: autopausebyothervideo, autopausebyvisibility
$GLOBALS['TCA'][$sModel]['palettes'] = array(

    'pallete_file_large' => array('showitem' => 'largemp4, --linebreak--, largewebm, --linebreak--, largeogv', 'canNotCollapse' => 1),
    'pallete_file_small' => array('showitem' => 'smallmp4, --linebreak--, smallwebm, --linebreak--, smallogv', 'canNotCollapse' => 0),
    'pallete_settings' => array('showitem' => 'preload, autoplay, controls, muted, ratio, playbyclick, width, loopvideo', 'canNotCollapse' => 1),
    'pallete_name_two' => array('showitem' => '', 'canNotCollapse' => 1),

);

//var_dump($GLOBALS['TCA'][$sModel]['palettes']);die;
$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';



$GLOBALS['TCA'][$sModel]['columns']['largemp4']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['largewebm']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['largeogv']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['smallmp4']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['smallwebm']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['smallogv']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['preload']['displayCond'] = 'FIELD:format:=:1';
$GLOBALS['TCA'][$sModel]['columns']['muted']['displayCond'] = 'FIELD:format:!=:2';
$GLOBALS['TCA'][$sModel]['columns']['videoid']['displayCond'] = 'FIELD:format:>:1';
$GLOBALS['TCA'][$sModel]['ctrl']['requestUpdate'] = 'format';
$GLOBALS['TCA'][$sModel]['columns']['format']['onchange'] = 'reload';

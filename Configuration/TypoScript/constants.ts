
plugin.tx_hivecptcntvideo_hivevideoshowvideo {
    view {
        # cat=plugin.tx_hivecptcntvideo_hivevideoshowvideo/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_video/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntvideo_hivevideoshowvideo/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_video/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntvideo_hivevideoshowvideo/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_video/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntvideo_hivevideoshowvideo//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
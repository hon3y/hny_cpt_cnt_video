
var hive_cpt_cnt_video__interval = setInterval(function () {
    if (typeof jQuery == 'undefined') {
    }  else {
        if (typeof Froogaloop == 'undefined') {
        }  else {
            clearInterval(hive_cpt_cnt_video__interval);

            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.log('init video');

            }

            init_hive_cpt_cnt_video();
        }
    }
}, 500);


function init_hive_cpt_cnt_video() {
    handleVimeoPoster();
    handleClickOnVideoElement();
}

function handleVimeoPoster() {
    var $last_hover;
    // for embedded ones
    $('.video--vimeo__wrapper').mousedown(function() {
        // hide poster
        $(this).find('.poster').fadeOut();
    });
    // note what we hover
    $('.video--vimeo__wrapper').hover(function() {
        $last_hover = $(this);
    }, function() {
        $last_hover = null;
    });
    // iframe is active by click
    $(window).blur(function() {
        $last_hover && $last_hover.find('.poster').fadeOut();
    });
    // specific video
    //var vimeo   = $('#vimeo')[0],
    //    $fvimeo = $f(vimeo);
    //$fvimeo.addEvent('ready', function() {
    //    $fvimeo.addEvent('play', function() {
    //        $(vimeo).next('.poster').fadeOut();
    //    });
    //});
}

function handleClickOnVideoElement() {
    $("video").click(function(e){
        // handle click if not Firefox (Firefox supports this feature natively)
        if (typeof InstallTrigger === 'undefined') {
            // get click position
            var clickY = (e.pageY - $(this).offset().top);
            var height = parseFloat( $(this).height() );
            // avoids interference with controls
            if (clickY > 0.82*height) return;
            // toggles play / pause
            this.paused ? this.play() : this.pause();
        }
    });
}

//  TODO: handle youtube posters
//function handleYoutubePoster() {
//    // poster frame click event
//    $(document).on('click','.js-videoPoster',function(ev) {
//        ev.preventDefault();
//        var $poster = $(this);
//        var $wrapper = $poster.closest('.js-videoWrapper');
//        videoPlay($wrapper);
//    });
//
//// play the targeted video (and hide the poster frame)
//    function videoPlay($wrapper) {
//        var $iframe = $wrapper.find('.js-videoIframe');
//        var src = $iframe.data('src');
//        // hide poster
//        $wrapper.addClass('videoWrapperActive');
//        // add iframe src in, starting the video
//        $iframe.attr('src',src);
//    }
//
//// stop the targeted/all videos (and re-instate the poster frames)
//    function videoStop($wrapper) {
//        // if we're stopping all videos on page
//        if (!$wrapper) {
//            var $wrapper = $('.js-videoWrapper');
//            var $iframe = $('.js-videoIframe');
//            // if we're stopping a particular video
//        } else {
//            var $iframe = $wrapper.find('.js-videoIframe');
//        }
//        // reveal poster
//        $wrapper.removeClass('videoWrapperActive');
//        // remove youtube link, stopping the video from playing in the background
//        $iframe.attr('src','');
//    }
//}
//
//function getYoutubeAPI() {
//    // Inject YouTube API script
//    var tag = document.createElement('script');
//    tag.src = "//www.youtube.com/player_api";
//    var firstScriptTag = document.getElementsByTagName('script')[0];
//    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
//}
//
//function onYouTubePlayerAPIReady() {
//
//}